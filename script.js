// 1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
// Если у нас есть объект и нам нужно взять его свойства и методы но мы не хотим их переписывать нам поможет прототипное наследование
// 2. Для чого потрібно викликати super() у конструкторі класу-нащадка?
// Простыми словами супер вызывает конструктор родителя


class Employee{
    constructor(name,age,salary){
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    get name(){
        return this._name
    }

    set name(value){
        this._name = value;
    }
    get age(){
        return this._age;
    }

    set age(value){
        this._age = value;
    }
    
    get salary(){
        return this._salary
    }
    set salary(value){
        this._salary = value;
    }
}

class Programmer extends Employee{
    constructor(name,age,salary,lang){
        super(name,age,salary,lang)
        this.lang = lang;
    }
    get salary(){
        return this._salary * 3;
    }
    set salary(value){
        this._salary = value;
    }
}

let programmer = new Programmer("Maxim", 20, 20000, 'Eng,german')
console.log(programmer);
console.log(programmer.salary);


let programmer1 = new Programmer('Bogdan', 25, 10000, 'Eng,France')
console.log(programmer1);
console.log(programmer1.salary);


let programmer2 = new Programmer('Anya', 15, 5000, 'Russian,Ukrain')
console.log(programmer2);
console.log(programmer2.salary);







